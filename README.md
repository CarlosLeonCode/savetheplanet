# 🎨 CSS Hover FX  

A collection of **pure CSS hover effects** to elegantly display captions or titles over images.  

✨ **Features:**  
- No JavaScript, just CSS  
- Smooth transitions and modern effects  
- Fully responsive and easy to customize  

## 🚀 Demo  
🔗 **View on CodePen:**  
[🔹 CSS Hover FX on CodePen](https://codepen.io/Robotlogy/pen/ExjLNMQ)  

## 📜 License  
MIT – Feel free to use and modify!  

---
Made with ❤️ by [CarlosLeonCode](https://carlosleoncode.com)